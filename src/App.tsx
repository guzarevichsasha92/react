import { useEffect } from "react";
import {
  BrowserRouter as Router,
  Navigate,
  Route,
  Routes,
} from "react-router-dom";
import { SkeletonTheme } from "react-loading-skeleton";
import { onAuthStateChanged } from "firebase/auth";

import { useAppDispatch, useAppSelector } from "./app/hooks";
import { logInUser, logOutUser } from "./features/user/userSlice";
import { auth } from "./services/firebaseConfig";

import ThemeProvider from "./contexts/theme/ThemeProvider";

import RootLayout from "./layouts/Root/RootLayout";
import MainLayout from "./layouts/Main/MainLayout";

import Home from "./pages/home/Home/Home";
import Login from "./pages/login/Login/Login";
import Menu from "./pages/menu/Menu/Menu";
import ProductGrid from "./pages/menu/ProductGrid/ProductGrid";
import Order from "./pages/order/Order/Order";
import NotFound from "./pages/notFound/NotFound/NotFound";
import SignUp from "./pages/signUp/SignUp/SignUp";

export default function App() {
  const isLoggedIn = useAppSelector((state) => state.user.isLoggedIn);
  const dispatch = useAppDispatch();

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        if (user.emailVerified && user.email) dispatch(logInUser(user.email));
      } else {
        dispatch(logOutUser());
      }
    });
  }, []);

  return (
    <Router>
      <ThemeProvider>
        <RootLayout>
          <SkeletonTheme baseColor="#f1f1f1" highlightColor="#ccc">
            <Routes>
              <Route path="/" element={<MainLayout />}>
                <Route index element={<Home />} />
                <Route
                  path="/menu"
                  element={<Menu children={<ProductGrid />} />}
                />
                <Route path="/login" element={<Login />} />
                <Route
                  path="/order"
                  element={isLoggedIn ? <Order /> : <Navigate to="/login" />}
                />
                <Route path="/signin" element={<SignUp />} />
              </Route>
              <Route path="*" element={<NotFound />} />
            </Routes>
          </SkeletonTheme>
        </RootLayout>
      </ThemeProvider>
    </Router>
  );
}
