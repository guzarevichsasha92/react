import { configureStore } from "@reduxjs/toolkit";
import productsReducer from "../features/products/productsSlice";
import { menuApi } from "../features/menuApi/menuApiSlice";
import userReducer from "../features/user/userSlice";

export const store = configureStore({
  reducer: {
    products: productsReducer,
    user: userReducer,
    [menuApi.reducerPath]: menuApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(menuApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
