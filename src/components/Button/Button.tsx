import React from "react";
import "./Button.css";

type ButtonProps = {
  onClick?: (e: React.FormEvent<HTMLButtonElement>) => void;
  text: string;
  primary?: boolean;
};

function Button({ onClick, text, primary = false }: ButtonProps) {
  const className = primary ? "primary" : "";
  return (
    <button onClick={onClick} className={`main__button ${className}`}>
      {text}
    </button>
  );
}

export default Button;
