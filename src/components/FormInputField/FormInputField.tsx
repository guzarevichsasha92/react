import React, { useEffect, useState } from "react";
import "./FormInputField.css";
import hidePassword from "../../assets/hide_password.svg";
import showPassword from "../../assets/show_password.svg";
import { preloadImages } from "../../utils/preloadImages";

export default function FormInputField({
  id,
  type = "text",
  onChange,
  value,
  name,
  text = "",
}: {
  id: string;
  type?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  name: string;
  text?: string;
}) {
  const [passwordVisibility, setPasswordVisibility] = useState(false);

  useEffect(() => {
    preloadImages(hidePassword, showPassword);
  }, []);

  function togglePasswordVisibility(e: React.MouseEvent<HTMLImageElement>) {
    e.stopPropagation();
    setPasswordVisibility((prev) => !prev);
  }

  return (
    <label htmlFor={id} className="form-input-field">
      <span>{text}</span>
      <input
        id={id}
        value={value}
        onChange={onChange}
        type={passwordVisibility ? "text" : type}
        name={name}
        className="input-field"
      />
      {type === "password" && (
        <img
          onClick={togglePasswordVisibility}
          className="toggle-password-visibility"
          src={passwordVisibility ? showPassword : hidePassword}
          alt={passwordVisibility ? "Show password" : "Hide password"}
        />
      )}
    </label>
  );
}
