import { Link, NavLink } from "react-router-dom";
import "./Header.css";
import { navBar } from "../../utils/constants";
import logoImage from "../../assets/logo.svg";
import cartImg from "../../assets/Framecart.svg";
import lightThemeIcon from "../../assets/light-theme-icon.svg";
import darkThemeIcon from "../../assets/dark-theme-icon.svg";
import { useContext, useEffect, useState } from "react";
import { useAppSelector } from "../../app/hooks";
import { ThemeContext } from "../../contexts/theme/ThemeProvider";
import { TThemeColorContext } from "../../types/global.types";
import {preloadImages} from "../../utils/preloadImages";


export default function Header() {
  const [isOpen, setIsOpen] = useState(false);
  const quantity = useAppSelector((state) => state.products.quantity);
  const user = useAppSelector((state) => state.user);
  const { themeColor, toggleTheme } = useContext(
    ThemeContext,
  ) as TThemeColorContext;

  useEffect(() => {
    preloadImages(lightThemeIcon, darkThemeIcon);
  }, []);

  function showEmail(link: string) {
    const username = user && user.email ? user.email.replace(/@.+/g, "") : "";
    if (link === "Login") {
      return user && user.isLoggedIn ? username : link;
    }
    return link;
  }

  function handleMenuToggle() {
    setIsOpen((prev) => !prev);
    document.body.classList.toggle("no-scroll");
  }

  function handleNavlinkClick() {
    setIsOpen(false);
    document.body.classList.remove("no-scroll");
  }

  return (
    <header className="header theme-background">
      <img src={logoImage} alt="logo" className="logo" />
      <div className="header__actions">
        <img
          className="theme-toggle"
          onClick={toggleTheme}
          src={themeColor === "dark" ? darkThemeIcon : lightThemeIcon}
          alt={`${themeColor} theme`}
        />

        <section className="cart">
          <Link to="/order">
            <img src={cartImg} alt="cart" className="cart__img" />
          </Link>
          <div className="cart__item-count">
            <p>{quantity > 99 ? "99+" : quantity}</p>
          </div>
        </section>
        <section>
          <div
            onClick={handleMenuToggle}
            className={isOpen ? "burger-menu opened" : "burger-menu"}
          >
            <div className="burger-menu__line"></div>
          </div>

          <nav className="navbar">
            {navBar.map((link) => (
              <NavLink
                onClick={handleNavlinkClick}
                key={link}
                to={`/${link === "Home" ? "" : link.toLowerCase()}`}
                className="navbar__element"
              >
                {showEmail(link)}
              </NavLink>
            ))}
          </nav>
        </section>
      </div>
    </header>
  );
}
