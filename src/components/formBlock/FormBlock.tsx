import "./FormBlock.css";
import FormInputField from "../FormInputField/FormInputField";
import Button from "../Button/Button";
import React, { useId } from "react";

type FormBlockProps = {
  emailHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
  emailValue: string;
  passwordHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
  passwordValue: string;
  submitHandler: (e: React.FormEvent<HTMLButtonElement>) => void;
  cancelHandler: (e: React.FormEvent<HTMLButtonElement>) => void;
  children?: React.ReactNode;
};

export default function FormBlock({
  emailHandler,
  emailValue,
  passwordHandler,
  passwordValue,
  submitHandler,
  cancelHandler,
  children,
}: FormBlockProps) {
  const id = useId();

  return (
    <section className="form-block-wrapper">
      <form className="form-block">
        <FormInputField
          id={`email-${id}`}
          type="email"
          onChange={emailHandler}
          value={emailValue}
          name="email"
          text="Email"
        />
        <FormInputField
          id={`password-${id}`}
          type="password"
          onChange={passwordHandler}
          value={passwordValue}
          name="password"
          text="Password"
        />
        <div className="button-group">
          <Button onClick={submitHandler} text="Submit" primary={true} />
          <Button onClick={cancelHandler} text="Cancel" primary={false} />
        </div>
      </form>

      {children}
    </section>
  );
}
