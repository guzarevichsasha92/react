import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ChosenProduct } from "../../types/global.types";

type Product = { products: ChosenProduct[]; quantity: number };
const initialState: Product = {
  products: [],
  quantity: 0,
};

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    addProduct(state, action: PayloadAction<ChosenProduct>) {
      const product = state.products
        .map((product) => product.id)
        .findIndex((id) => id === action.payload.id);
      if (product !== -1) {
        state.products[product].quantity += action.payload.quantity;
      } else {
        state.products.push(action.payload);
      }
      state.quantity += action.payload.quantity;
    },
    removeProduct(state, action: PayloadAction<ChosenProduct>) {
      const index = state.products.findIndex(
        (product) => product.id === action.payload.id,
      );
      state.products.splice(index, 1);
      state.quantity -= action.payload.quantity;
    },
    updateQuantity(
      state,
      action: PayloadAction<{ product: ChosenProduct; quantity: number }>,
    ) {
      const productIndex = state.products.findIndex(
        (product) => product.id === action.payload.product.id,
      );
      state.quantity =
        state.quantity -
        state.products[productIndex].quantity +
        action.payload.quantity;
      state.products[productIndex].quantity = action.payload.quantity;
    },
  },
});

// remove Produce will be used for Order page, where it will be available to remove items
export const { addProduct, removeProduct, updateQuantity } =
  productsSlice.actions;

export default productsSlice.reducer;
