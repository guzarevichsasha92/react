import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import { ReactElement } from "react";
import useIsBack from "../../hooks/useIsBack";

export default function RootLayout({ children }: { children: ReactElement }) {
  const isBack = useIsBack();
  if (!isBack) {
    window.scrollTo(0, 0);
  }
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
}
