import React, { useEffect, useState } from "react";
import "./Login.css";
import {
  signInWithEmailAndPassword,
  signInWithPopup,
  getRedirectResult,
} from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { auth, provider } from "../../../services/firebaseConfig";
import { logInUser } from "../../../features/user/userSlice";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import Button from "../../../components/Button/Button";
import googleIcon from "../../../assets/google-svgrepo-com.svg";
import FormBlock from "../../../components/formBlock/FormBlock";

export default function Login() {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const isLoggedIn = useAppSelector((state) => state.user.isLoggedIn);

  useEffect(() => {
    getRedirectResult(auth)
      .then((result) => {
        if (result) {
          const user = result.user;
          if (user.email) {
            dispatch(logInUser(user.email));
          }
          navigate("/");
        }
      })
      .catch((error) => {
        setMessage("An error occurred. Please try again.");
        console.error(error.code, error.message);
      });
  }, [dispatch, navigate]);

  async function handleSubmit(e: React.FormEvent<HTMLButtonElement>) {
    e.preventDefault();
    setPassword("");
    setEmail("");

    try {
      const userCredential = await signInWithEmailAndPassword(
        auth,
        email,
        password,
      );
      if (!userCredential.user.emailVerified) {
        setMessage("Please verify your email address.");
        return;
      }
      dispatch(logInUser(email));
      navigate("/");
    } catch (error) {
      setMessage("Invalid credentials. Please try again.");
      // @ts-ignore
      console.error(error.code, error.message);
    }
  }

  async function handleGoogleAuth() {
    try {
      const result = await signInWithPopup(auth, provider);
      const user = result.user;
      if (user.email) {
        dispatch(logInUser(user.email));
      }
      navigate("/");
    } catch (error) {
      setMessage("Google sign-in failed. Please try again.");
      // @ts-ignore
      console.error(error.code, error.message);
    }
  }

  function handleCancel(e: React.FormEvent<HTMLButtonElement>) {
    e.preventDefault();
    setEmail("");
    setPassword("");
  }

  function handleLogOut() {
    auth.signOut();
    navigate("/");
  }

  if (isLoggedIn) {
    return <Button text="Log out" primary={true} onClick={handleLogOut} />;
  }

  return (
    <>
      <h2 className="main__heading">Log in</h2>
      <FormBlock
        emailHandler={(e) => setEmail(e.target.value)}
        emailValue={email}
        passwordHandler={(e) => setPassword(e.target.value)}
        passwordValue={password}
        submitHandler={handleSubmit}
        cancelHandler={handleCancel}
      >
        <div className="google-auth" onClick={handleGoogleAuth}>
          <img src={googleIcon} alt="google" />
        </div>
        <Button
          text="Sign Up"
          primary={true}
          onClick={() => navigate("/signin")}
        />

        <p style={{ display: message ? "block" : "none" }} className="error">
          {message}
        </p>
      </FormBlock>
    </>
  );
}
