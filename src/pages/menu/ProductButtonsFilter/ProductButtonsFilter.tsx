import Button from "../../../components/Button/Button";
import "./ProductButtonsFilter.css";

type ProductButtonsFilterProps = {
  handleFilter: (category: string) => void;
  filterCategory: string;
};

function ProductButtonsFilter({
  handleFilter,
  filterCategory,
}: ProductButtonsFilterProps) {
  return (
    <div className="main__filter-buttons">
      <Button
        primary={filterCategory === "Dessert"}
        text="Dessert"
        onClick={() => handleFilter("Dessert")}
      />
      <Button
        primary={filterCategory === "Dinner"}
        text="Dinner"
        onClick={() => handleFilter("Dinner")}
      />
      <Button
        primary={filterCategory === "Breakfast"}
        text="Breakfast"
        onClick={() => handleFilter("Breakfast")}
      />
    </div>
  );
}

export default ProductButtonsFilter;
