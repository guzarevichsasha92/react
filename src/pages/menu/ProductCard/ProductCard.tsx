import React, { useState } from "react";
import "./ProductCard.css";
import Button from "../../../components/Button/Button";
import type { MenuType } from "../../../types/global.types";
import Skeleton from "react-loading-skeleton";
import { useAppDispatch } from "../../../app/hooks";
import { addProduct } from "../../../features/products/productsSlice";
import ProductCountButton from "../../../components/ProductCount/ProductCount";
import { validateQuantityInput } from "../../../utils/validateQuantityInput";

type ProductCardProps = MenuType;

function ProductCard({ id, img, meal, price, instructions }: ProductCardProps) {
  const [quantity, setQuantity] = useState<number>(1);
  const [isDescriptionVisible, setIsDescriptionVisible] = useState<
    "opened" | ""
  >("");
  const [isImageLoaded, setIsImageLoaded] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  function handleQuantity(e: React.ChangeEvent<HTMLInputElement>) {
    const numericValue = validateQuantityInput(e);
    setQuantity(Math.min(99, numericValue)); // Limits to 99 as max productsQuantity
  }

  function processAddToCart() {
    if (quantity > 0) {
      const newProducts = {
        id,
        quantity,
      };
      dispatch(addProduct(newProducts));
    }
  }

  return (
    <div className={`content-block ${isDescriptionVisible}`}>
      {!isImageLoaded && (
        <Skeleton
          containerClassName="content-block__image"
          width={120}
          height={120}
        />
      )}
      <img
        src={img}
        alt={meal}
        style={{ display: isImageLoaded ? "inline-block" : "none" }}
        onLoad={() => setIsImageLoaded(true)}
        className="content-block__image"
      />
      <div className="content-block__item-wrapper">
        <div className="content-block__item-info">
          <span className="content-block__name">{meal}</span>
          <span className="content-block__price">${price.toFixed(2)}</span>
        </div>
        <div
          onClick={() =>
            setIsDescriptionVisible(isDescriptionVisible ? "" : "opened")
          }
          className="content-block__item-description"
        >
          {instructions}
        </div>
        <div className="content-block__item-cart">
          <ProductCountButton quantity={quantity} onChange={handleQuantity} />
          <Button
            onClick={processAddToCart}
            primary={true}
            text="Add to cart"
          />
        </div>
      </div>
    </div>
  );
}

export default ProductCard;
