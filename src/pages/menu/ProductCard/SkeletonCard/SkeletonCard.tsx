import "../ProductCard.css";
import Button from "../../../../components/Button/Button";
import Skeleton from "react-loading-skeleton";
import ProductCount from "../../../../components/ProductCount/ProductCount";

export default function SkeletonCard() {
  return (
    <div className="content-block">
      <Skeleton
        containerClassName="content-block__image"
        width={120}
        height={120}
      />
      <div className="content-block__item-wrapper">
        <div className="content-block__item-info">
          <Skeleton className="content-block__name" width={120} />
          <Skeleton className="content-block__price" width={30} />
        </div>
        <Skeleton
          containerClassName="content-block__skeleton-item-description"
          count={3}
        />
        <div className="content-block__item-cart">
          <ProductCount quantity={1} disabled={true}/>
          <Button primary={true} text="Add to cart" />
        </div>
      </div>
    </div>
  );
}
