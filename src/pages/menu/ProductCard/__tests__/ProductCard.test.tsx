import { render, screen, fireEvent } from "@testing-library/react";
import ProductCard from "../ProductCard";
import { Provider } from "react-redux";
import { useAppDispatch } from "../../../../app/hooks";
import { store } from "../../../../app/store";
import { addProduct } from "../../../../features/products/productsSlice";
import { MenuType } from "../../../../types/global.types";

jest.mock("../../../../app/hooks");

const mockDispatch = jest.fn();
(useAppDispatch as jest.MockedFunction<typeof useAppDispatch>).mockReturnValue(
  mockDispatch,
);

const product: MenuType = {
  id: "1",
  img: "image-url",
  meal: "Test Meal",
  price: 10.5,
  instructions: "Test instructions",
  category: "Main Course",
  area: "Italian",
};

function renderComponent() {
  render(
    <Provider store={store}>
      <ProductCard {...product} />
    </Provider>,
  );
}

describe("ProductCard", () => {
  beforeEach(() => {
    mockDispatch.mockClear();
  });

  it("should render product details correctly", () => {
    renderComponent();
    expect(screen.getByText("Test Meal")).toBeInTheDocument();
    expect(screen.getByText("$10.50")).toBeInTheDocument();
    expect(screen.getByText("Test instructions")).toBeInTheDocument();
  });

  it("should show skeleton loader before image loads", () => {
    renderComponent();
    expect(screen.getByRole("img", { hidden: true })).toHaveStyle({
      display: "none",
    });
  });

  it("should display image after it loads", () => {
    renderComponent();
    const img = screen.getByAltText("Test Meal");
    fireEvent.load(img);
    expect(img).toHaveStyle({ display: "inline-block" });
  });

  it("should handle quantity change", () => {
    renderComponent();
    const input = screen.getByRole("spinbutton") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "5" } });
    expect(input.value).toBe("5");
  });

  it("should add product to cart", () => {
    renderComponent();
    const button = screen.getByText("Add to cart");
    fireEvent.click(button);
    expect(mockDispatch).toHaveBeenCalledWith({
      type: addProduct.type,
      payload: { id: "1", quantity: 1 },
    });
  });

  it("should limit quantity to 99", () => {
    renderComponent();
    const input = screen.getByRole("spinbutton") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "100" } });
    expect(input.value).toBe("99");
  });
});
