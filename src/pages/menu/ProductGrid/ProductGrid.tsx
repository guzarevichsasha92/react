import { useEffect, useState } from "react";
import "./ProductGrid.css";
import Button from "../../../components/Button/Button";
import ProductCard from "../ProductCard/ProductCard";
import ProductButtonsFilter from "../ProductButtonsFilter/ProductButtonsFilter";
import SkeletonCard from "../ProductCard/SkeletonCard/SkeletonCard";
import { useGetMenuQuery } from "../../../features/menuApi/menuApiSlice";
import { MenuType } from "../../../types/global.types";

function ProductGrid() {
  const { data, isLoading, error } = useGetMenuQuery();
  const [amountOfDisplayedItems, setAmountOfDisplayedItems] =
    useState<number>(6);
  const [filterCategory, setFilterCategory] = useState<string>("Dessert");
  const [filteredData, setFilteredData] = useState<Array<MenuType> | null>(
    null,
  );
  const itemsToDisplay = 6;

  useEffect(() => {
    if (data) {
      setFilteredData(
        data.filter((product) => product.category === filterCategory),
      );
    }
  }, [JSON.stringify(data), filterCategory]);

  const handleFilter = (filter: string) => {
    setFilterCategory(filter);
    setAmountOfDisplayedItems(itemsToDisplay);
  };

  const handleSeeMore = () => {
    setAmountOfDisplayedItems((prev) => prev + itemsToDisplay);
  };

  function showItems() {
    if (filteredData) {
      return filteredData
        .slice(0, amountOfDisplayedItems)
        .map((item) => <ProductCard key={item.id} {...item} />);
    }
    return null;
  }

  if (error) {
    return <p>Oops, It seems we have an error</p>;
  }

  return (
    <>
      <section className="content-wrapper">
        <ProductButtonsFilter
          filterCategory={filterCategory}
          handleFilter={handleFilter}
        />

        <section className="content">
          {isLoading
            ? Array.from({ length: 6 }).map((_, index) => (
                <SkeletonCard key={index} />
              ))
            : showItems()}
        </section>
        {filteredData && filteredData.length > amountOfDisplayedItems && (
          <Button primary={true} text="See more" onClick={handleSeeMore} />
        )}
      </section>
    </>
  );
}

export default ProductGrid;
