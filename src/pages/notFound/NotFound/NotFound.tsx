import { Link, useNavigate } from "react-router-dom";
import "./NotFound.css";
import notFound from "../../../assets/not-found.webp";

export default function NotFound() {
  const navigate = useNavigate();

  return (
    <article className="not-found">
      <h2 className="main__heading">Page not found. 404</h2>
      <img src={notFound} alt="sad sticker" />
      <div>
        <Link to="/">Home</Link>
        <Link
          to={".."}
          onClick={(e) => {
            e.preventDefault();
            navigate(-1);
          }}
        >
          Back
        </Link>
      </div>
    </article>
  );
}
