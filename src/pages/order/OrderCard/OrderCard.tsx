import "./OrderCard.css";
import { ChosenProduct, MenuType } from "../../../types/global.types";
import ProductCount from "../../../components/ProductCount/ProductCount";
import {
  removeProduct,
  updateQuantity,
} from "../../../features/products/productsSlice";
import Button from "../../../components/Button/Button";
import React, { useMemo, useState } from "react";
import { useGetMenuQuery } from "../../../features/menuApi/menuApiSlice";
import { useAppDispatch } from "../../../app/hooks";
import { validateQuantityInput } from "../../../utils/validateQuantityInput";

type OrderCardChildProps = {
  chosenProduct: ChosenProduct;
};

export function OrderCard({ chosenProduct }: OrderCardChildProps) {
  const [quantity, setQuantity] = useState(chosenProduct.quantity);
  const { data, error } = useGetMenuQuery();
  const dispatch = useAppDispatch();

  // explicit usage of data!, cause this page is private and items will be displayed only if a cart has some products,
  // otherwise data will not be used, so when a user goes to menu page data is loaded
  const product = useMemo(
    () => data!.find((product) => product.id === chosenProduct.id) as MenuType,
    [data],
  );
  // useMemo is used to prevent recalculations on every rerender

  function handleQuantity(e: React.ChangeEvent<HTMLInputElement>) {
    const numericValue = validateQuantityInput(e);
    setQuantity(Math.min(99, numericValue)); // Limits to 99 as max productsQuantity
  }

  if (error) {
    return <p>Oops, It seems we have an error</p>;
  }

  return (
    <div className="order__card" key={chosenProduct.id}>
      <img src={product.img} alt={product.meal} />
      <div className="order__card-tools">
        <p>{product.meal}</p>
        <span className="blue-link">
          $ {Math.round(product.price * chosenProduct.quantity * 100) / 100}
          USD
        </span>
        <div className="order__card__action-buttons">
          <ProductCount
            onBlur={() =>
              dispatch(
                updateQuantity({ product: chosenProduct, quantity: quantity }),
              )
            }
            quantity={quantity}
            onChange={handleQuantity}
          />
          <Button
            text="X"
            onClick={() => dispatch(removeProduct(chosenProduct))}
            primary={true}
          />
        </div>
      </div>
    </div>
  );
}
