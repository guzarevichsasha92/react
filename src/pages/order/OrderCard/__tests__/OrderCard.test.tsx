import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import { OrderCard } from "../OrderCard";
import { MemoryRouter } from "react-router-dom";
import {
  removeProduct,
  updateQuantity,
} from "../../../../features/products/productsSlice";
import { useAppDispatch } from "../../../../app/hooks";
import { ChosenProduct } from "../../../../types/global.types";
import { ThemeContext } from "../../../../contexts/theme/ThemeProvider";
import {
  menuApi,
  useGetMenuQuery,
} from "../../../../features/menuApi/menuApiSlice";
import productsReducer from "../../../../features/products/productsSlice";

jest.mock("../../../../app/hooks", () => ({
  ...jest.requireActual("../../../../app/hooks"),
  useAppDispatch: jest.fn(),
}));

jest.mock("../../../../features/menuApi/menuApiSlice", () => ({
  ...jest.requireActual("../../../../features/menuApi/menuApiSlice"),
  useGetMenuQuery: jest.fn(),
}));

const mockDispatch = jest.fn();
(useAppDispatch as jest.MockedFunction<typeof useAppDispatch>).mockReturnValue(
  mockDispatch,
);

const mockMenuData = [
  {
    id: "1",
    meal: "Test Meal",
    img: "test-image-url",
    price: 10,
  },
];

(useGetMenuQuery as jest.Mock).mockReturnValue({
  data: mockMenuData,
  error: null,
  isLoading: false,
});

const chosenProduct: ChosenProduct = {
  id: "1",
  quantity: 1,
};

function configureTestStore() {
  return configureStore({
    reducer: {
      products: productsReducer,
      [menuApi.reducerPath]: menuApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(menuApi.middleware),
  });
}

function renderComponent(product: ChosenProduct) {
  const testStore = configureTestStore();
  return render(
    <MemoryRouter>
      <Provider store={testStore}>
        <ThemeContext.Provider
          value={{ themeColor: "dark", toggleTheme: jest.fn() }}
        >
          <OrderCard chosenProduct={product} />
        </ThemeContext.Provider>
      </Provider>
    </MemoryRouter>,
  );
}

describe("OrderCard Component", () => {
  beforeEach(() => {
    mockDispatch.mockClear();
  });

  it("should render the order card with product details", () => {
    renderComponent(chosenProduct);

    expect(screen.getByAltText("Test Meal")).toBeInTheDocument();
    expect(screen.getByText("Test Meal")).toBeInTheDocument();
    expect(screen.getByText("$ 10USD")).toBeInTheDocument();
  });

  it("should update the quantity when changed", () => {
    renderComponent(chosenProduct);

    const input = screen.getByRole("spinbutton");
    fireEvent.change(input, { target: { value: "3" } });
    fireEvent.blur(input);

    expect(mockDispatch).toHaveBeenCalledWith(
      updateQuantity({ product: chosenProduct, quantity: 3 }),
    );
  });

  it("should remove the product when remove button is clicked", () => {
    renderComponent(chosenProduct);

    const removeButton = screen.getByText("X");
    fireEvent.click(removeButton);

    expect(mockDispatch).toHaveBeenCalledWith(removeProduct(chosenProduct));
  });
});
