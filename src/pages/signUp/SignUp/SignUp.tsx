import "./SignUp.css";
import { FormEvent, useState } from "react";
import FormBlock from "../../../components/formBlock/FormBlock";
import {
  sendEmailVerification,
  createUserWithEmailAndPassword,
} from "firebase/auth";
import { auth } from "../../../services/firebaseConfig";
import { useNavigate } from "react-router-dom";

export default function SignUp() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isPasswordValid, setIsPasswordValid] = useState<boolean | null>(null);
  const [errorMessage, setErrorMessage] = useState<string>("");
  const navigate = useNavigate();

  const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@!$#%^&*]).{8,}$/;

  function isValidPassword(password: string): boolean {
    return passwordRegex.test(password);
  }

  function handleCancel(e: FormEvent<HTMLButtonElement>) {
    e.preventDefault();
    setEmail("");
    setPassword("");
  }

  async function handleSubmit(e: FormEvent<HTMLButtonElement>) {
    e.preventDefault();
    setIsPasswordValid(true);

    if (!isValidPassword(password)) {
      setIsPasswordValid(false);
      return;
    }

    try {
      const userCredential = await createUserWithEmailAndPassword(
        auth,
        email,
        password,
      );
      await sendEmailVerification(userCredential.user);
      console.log("after sending");
      console.log("Email verification sent");
      navigate("/login");
    } catch (error) {
      // @ts-ignore
      if (typeof error.message === "string") {
        console.log(error);
        // @ts-ignore
        setErrorMessage(error.message);
      }

      setPassword("");
      setEmail("");
    }
  }

  return (
    <>
      <h2 className="main__heading">Sign Up</h2>
      <FormBlock
        emailHandler={(e) => setEmail(e.target.value)}
        emailValue={email}
        passwordHandler={(e) => setPassword(e.target.value)}
        passwordValue={password}
        submitHandler={handleSubmit}
        cancelHandler={handleCancel}
      >
        <p className="email-check-notify">Check your email box after submit</p>
        <p
          className={`weak-password ${isPasswordValid === false ? "show" : ""}`}
        >
          Your password is weak
        </p>
        <p
          style={{ display: errorMessage ? "block" : "none" }}
          className="sign-up-error"
        >
          {errorMessage}
        </p>
      </FormBlock>
    </>
  );
}
