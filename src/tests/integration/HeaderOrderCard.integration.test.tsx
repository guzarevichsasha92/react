import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import Header from "../../components/Header/Header";
import { configureStore } from "@reduxjs/toolkit";
import productsReducer, {
  addProduct,
  updateQuantity,
  removeProduct,
} from "../../features/products/productsSlice";
import { OrderCard } from "../../pages/order/OrderCard/OrderCard";
import { MemoryRouter } from "react-router-dom";
import { ThemeContext } from "../../contexts/theme/ThemeProvider";
import { act } from "react-dom/test-utils";
import { useAppDispatch } from "../../app/hooks";
import { ChosenProduct } from "../../types/global.types";
import { menuApi } from "../../features/menuApi/menuApiSlice";
import { useGetMenuQuery } from "../../features/menuApi/menuApiSlice"; // Import the hook

jest.mock("../../app/hooks", () => ({
  ...jest.requireActual("../../app/hooks"),
  useAppDispatch: jest.fn(),
}));

jest.mock("../../features/menuApi/menuApiSlice", () => ({
  ...jest.requireActual("../../features/menuApi/menuApiSlice"),
  useGetMenuQuery: jest.fn(),
}));

const mockDispatch = jest.fn();
(useAppDispatch as jest.MockedFunction<typeof useAppDispatch>).mockReturnValue(
  mockDispatch,
);

const mockMenuData = [
  {
    id: "1",
    meal: "Test Meal",
    img: "test-image-url",
    price: 10,
  },
];

(useGetMenuQuery as jest.Mock).mockReturnValue({
  data: mockMenuData,
  error: null,
  isLoading: false,
});

const chosenProduct: ChosenProduct = {
  id: "1",
  quantity: 1,
};

function configureTestStore() {
  return configureStore({
    reducer: {
      products: productsReducer,
      [menuApi.reducerPath]: menuApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(menuApi.middleware),
  });
}

function renderComponents(testStore: any, product: ChosenProduct) {
  return render(
    <MemoryRouter>
      <Provider store={testStore}>
        <ThemeContext.Provider
          value={{ themeColor: "dark", toggleTheme: jest.fn() }}
        >
          <Header />
          <OrderCard chosenProduct={product} />
        </ThemeContext.Provider>
      </Provider>
    </MemoryRouter>,
  );
}

describe("OrderCard and Header Integration", () => {
  let testStore: ReturnType<typeof configureTestStore>;

  beforeEach(() => {
    testStore = configureTestStore();
    mockDispatch.mockClear();
  });

  it("should display the updated quantity in the header cart when quantity changes in OrderCard", () => {
    renderComponents(testStore, chosenProduct);
    // adding the product to the store
    act(() => {
      testStore.dispatch(addProduct(chosenProduct));
    });

    // Simulate changing the quantity in the OrderCard
    const input = screen.getByRole("spinbutton");
    fireEvent.change(input, { target: { value: "3" } });
    act(() => {
      testStore.dispatch(
        updateQuantity({ product: chosenProduct, quantity: 3 }),
      );
    });

    // Check if the header cart updates
    expect(screen.getByText("3")).toBeInTheDocument();
  });

  it("should remove the product from the header cart when removed in OrderCard", () => {
    renderComponents(testStore, chosenProduct);

    act(() => {
      testStore.dispatch(addProduct(chosenProduct));
    });

    // Simulate removing the product in the OrderCard
    const removeButton = screen.getByText("X");
    fireEvent.click(removeButton);
    act(() => {
      testStore.dispatch(removeProduct(chosenProduct));
    });

    // Check if the header cart updates
    expect(screen.getByText("0")).toBeInTheDocument();
  });
});
