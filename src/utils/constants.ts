export const navBar = ["Home", "Menu", "Company", "Login"];

export const footerColumns = [
  ["COMPANY", "Home", "Order", "FAQ", "Contact"],
  ["TEMPLATE", "Style Guide", "Licence", "Webflow University"],
  ["FLOWBASE", "More Cloneables"],
];
